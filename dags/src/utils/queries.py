# Total late fee amount reports

CREATE_TEMP_TABLE_ORDER_LATE_FEE_AMOUNT = """
create temporary table if not exists tamara_reports.order_late_fee_amount as (
    with order_total_removed_latefee as (
        select order_id,
               sum(json_extract(json_unquote(payload), '$.late_fee_amount.amount')) as total_removed_late_fee_amount
        from tamara.order_events
        where event_name like '%LateFeeWasRemoved%'
        group by order_id
    )
       , order_total_latefee as (
        select order_id,
               sum(json_extract(json_unquote(payload), '$.late_fee_amount.amount')) as total_late_fee_amount
        from tamara.order_events
        where event_name like '%OrderWasOverdue%'
        group by order_id
    )
    select ol.order_id,
           o.created_at,
           (total_late_fee_amount - coalesce(total_removed_late_fee_amount, 0)) as total_late_fee_amount
    from order_total_latefee ol
             left join order_total_removed_latefee orl on ol.order_id = orl.order_id
             left join tamara.orders o on ol.order_id = o.id_order);
"""

CREATE_TABLE_TOTAL_LATE_FEE_BY_DAY = """
create table if not exists tamara_reports.total_late_fee_by_day as (
    select date(created_at) as day, sum(total_late_fee_amount) as total_late_fee_amount
    from tamara_reports.order_late_fee_amount
    where total_late_fee_amount > 0
    group by day
    order by day desc
);
"""

CREATE_TABLE_TOTAL_LATE_FEE_BY_MONTH = """
create table if not exists tamara_reports.total_late_fee_by_month as (
    select  date_format(created_at, '%Y-%m') as month, 
            sum(total_late_fee_amount) as total_late_fee_amount
    from tamara_reports.order_late_fee_amount
    where total_late_fee_amount > 0
    group by month
    order by month desc
);
"""

CREATE_TABLE_TOTAL_LATE_FEE_BY_QUARTER = """
create table if not exists tamara_reports.total_late_fee_by_quarter as (
    select concat(year(created_at), '-', quarter(created_at)) as quarter,
           sum(total_late_fee_amount) as total_late_fee_amount
    from tamara_reports.order_late_fee_amount
    where total_late_fee_amount > 0
    group by quarter
    order by quarter desc
);
"""

CREATE_TABLE_TOTAL_LATE_FEE_BY_YEAR = """
create table if not exists tamara_reports.total_late_fee_by_year as (
    select year(created_at) as year, 
           sum(total_late_fee_amount) as total_late_fee_amount
    from tamara_reports.order_late_fee_amount
    where total_late_fee_amount > 0
    group by year
    order by year desc
);
"""

# Top 10 most purchased items reports
CREATE_TEMP_TABLE_ITEM_ORDER_INFO = """
create temporary table if not exists tamara_reports.item_order_info as (
    select event_id, 
           event_name,
           order_id,
           json_extract(json_unquote(payload), '$.items') as order_items
    from tamara.order_events
    where event_name like '%OrderWasCreated%'
);
"""

CREATE_TEMP_TABLE_DETAILED_ITEM_ORDER_INFO = """
create temporary table if not exists tamara_reports.detailed_item_order_info as (
    with exploded_item_order_info as (
        select *
        from tamara_reports.item_order_info pi
                 join json_table(
                pi.order_items,
                '$[*]' columns (order_item json path '$')
            ) t
    )
    select event_id,
           event_name,
           order_id,
           json_extract(order_item, '$.sku')                    as sku,
           json_extract(order_item, '$.quantity')               as quantity
    from exploded_item_order_info);
"""

CREATE_TABLE_TOP_10_MOST_PURCHASED_ITEM_BY_DAY = """
create table if not exists tamara_reports.top_10_most_purchased_items_by_day as (
    with top_10_item_purchased_by_date as (
        select *,
               ROW_NUMBER() over (
                   partition by day
                   order by total_purchased_quantity desc
                   ) rn
        from (
                 select date(o.created_at) as day,
                        sku,
                        sum(quantity)      as total_purchased_quantity
                 from tamara_reports.detailed_item_order_info di
                          join tamara.orders o on di.order_id = o.id_order
                 where status like '%captured%'
                 group by day, sku
             ) T
    )
    select day, sku, total_purchased_quantity
    from top_10_item_purchased_by_date
    where rn <= 10
    order by day desc, total_purchased_quantity desc
);
"""

CREATE_TABLE_TOP_10_MOST_PURCHASED_ITEM_BY_MONTH = """
create table if not exists tamara_reports.top_10_most_purchased_items_by_month as (
    with most_purchased_items_by_month as (
        select *,
               ROW_NUMBER() over (
                   partition by month
                   order by total_purchased_quantity desc
                   ) rn
        from (
                 select date_format(o.created_at, '%Y-%m') as month,
                        sku,
                        sum(quantity)      as total_purchased_quantity
                 from tamara_reports.detailed_item_order_info di
                          join tamara.orders o on di.order_id = o.id_order
                 where status like '%captured%'
                 group by month, sku
             ) T
    )
    select month, sku, total_purchased_quantity
    from most_purchased_items_by_month
    where rn <= 10
    order by month desc, total_purchased_quantity desc
);
"""

CREATE_TABLE_TOP_10_MOST_PURCHASED_ITEM_BY_QUARTER = """
create table if not exists tamara_reports.top_10_most_purchased_items_by_quarter as (
    with most_purchased_items_by_quarter as (
        select *,
               ROW_NUMBER() over (
                   partition by quarter
                   order by total_purchased_quantity desc
                   ) rn
        from (
                 select concat(year(o.created_at), '-', quarter(o.created_at)) as quarter,
                        sku,
                        sum(quantity)      as total_purchased_quantity
                 from tamara_reports.detailed_item_order_info di
                          join tamara.orders o on di.order_id = o.id_order
                 where status like '%captured%'
                 group by quarter, sku
             ) T
    )
    select quarter, sku, total_purchased_quantity
    from most_purchased_items_by_quarter
    where rn <= 10
    order by quarter desc, total_purchased_quantity desc
);
"""

CREATE_TABLE_TOP_10_MOST_PURCHASED_ITEM_BY_YEAR = """
create table if not exists tamara_reports.top_10_most_purchased_items_by_year as (
    with most_purchased_items_by_year as (
        select *,
               ROW_NUMBER() over (
                   partition by year
                   order by total_purchased_quantity desc
                   ) rn
        from (
                 select year(o.created_at) as year,
                        sku,
                        sum(quantity)      as total_purchased_quantity
                 from tamara_reports.detailed_item_order_info di
                          join tamara.orders o on di.order_id = o.id_order
                 where status like '%captured%'
                 group by year, sku
             ) T
    )
    select year, sku, total_purchased_quantity
    from most_purchased_items_by_year
    where rn <= 10
    order by year desc, total_purchased_quantity desc
);
"""

# Extract order info from payload
CREATE_TEMP_TABLE_ORDER_INFO = """
create temporary table if not exists tamara_reports.order_info as (
    select event_id,
           event_name,
           order_id,
           TRIM(BOTH '"' FROM json_extract(json_unquote(payload), '$.merchant_name')) as merchant_name,
           STR_TO_DATE(TRIM(BOTH '"' FROM json_extract(json_unquote(payload), '$.created_at')),
                       '%Y-%m-%dT%T+%H:%i')                                           as created_at,
           json_extract(json_unquote(payload), '$.total_amount.amount')               as order_total_amount,
           json_extract(json_unquote(payload), '$.total_amount.currency')             as order_currency
    from tamara.order_events
    where event_name like '%OrderWasCreated%'
);
"""

# Top 10 merchants who have most canceled value
CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_CANCELED_ORDER_VALUE_BY_DAY = """
create table if not exists tamara_reports.top_10_merchant_has_most_canceled_order_value_by_day as (
    with top_merchant_has_most_canceled_order_value_by_day as (
        select *,
               ROW_NUMBER() over (
                   partition by day
                   order by total_canceled_amount desc
                   ) rn
        from (
                 select date(oi.created_at)        as day,
                        oi.merchant_name,
                        oi.order_currency          as currency,
                        sum(oi.order_total_amount) as total_canceled_amount
                 from tamara.orders o
                          join tamara_reports.order_info oi on o.id_order = oi.order_id
                 where status = 'canceled'
                 group by day, merchant_name, currency
             ) T
    )
    select day,
           merchant_name,
           total_canceled_amount,
           currency
    from top_merchant_has_most_canceled_order_value_by_day tmd
    where tmd.rn <= 10
    order by day desc, total_canceled_amount desc
);
"""

CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_CANCELED_ORDER_VALUE_BY_MONTH = """
create table if not exists tamara_reports.top_10_merchant_has_most_canceled_order_value_by_month as (
    with top_merchant_has_most_canceled_order_value_by_month as (
        select *,
               ROW_NUMBER() over (
                   partition by month
                   order by total_canceled_amount desc
                   ) rn
        from (
                 select date_format(oi.created_at, '%Y-%m') as month,
                        oi.merchant_name,
                        oi.order_currency          as currency,
                        sum(oi.order_total_amount) as total_canceled_amount
                 from tamara.orders o
                          join tamara_reports.order_info oi on o.id_order = oi.order_id
                 where status = 'canceled'
                 group by month, merchant_name, currency
             ) T
    )
    select month,
           merchant_name,
           total_canceled_amount,
           currency
    from top_merchant_has_most_canceled_order_value_by_month tmm
    where tmm.rn <= 10
    order by month desc, total_canceled_amount desc
);
"""

CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_CANCELED_ORDER_VALUE_BY_QUARTER = """
create table if not exists tamara_reports.top_10_merchant_has_most_canceled_order_value_by_quarter as (
    with top_merchant_has_most_canceled_order_value_by_quarter as (
        select *,
               ROW_NUMBER() over (
                   partition by quarter
                   order by total_canceled_amount desc
                   ) rn
        from (
                 select concat(year(oi.created_at), '-', quarter(oi.created_at)) as quarter,
                        oi.merchant_name,
                        oi.order_currency          as currency,
                        sum(oi.order_total_amount) as total_canceled_amount
                 from tamara.orders o
                          join tamara_reports.order_info oi on o.id_order = oi.order_id
                 where status = 'canceled'
                 group by quarter, merchant_name, currency
             ) T
    )
    select quarter,
           merchant_name,
           total_canceled_amount,
           currency
    from top_merchant_has_most_canceled_order_value_by_quarter tmq
    where tmq.rn <= 10
    order by quarter desc, total_canceled_amount desc
);
"""

CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_CANCELED_ORDER_VALUE_BY_YEAR = """
create table if not exists tamara_reports.top_10_merchant_has_most_canceled_order_value_by_year as (
    with top_merchant_has_most_canceled_order_value_by_year as (
        select *,
               ROW_NUMBER() over (
                   partition by year
                   order by total_canceled_amount desc
                   ) rn
        from (
                 select year(oi.created_at)        as year,
                        oi.merchant_name,
                        oi.order_currency          as currency,
                        sum(oi.order_total_amount) as total_canceled_amount
                 from tamara.orders o
                          join tamara_reports.order_info oi on o.id_order = oi.order_id
                 where status = 'canceled'
                 group by year, merchant_name, currency
             ) T
    )
    select year,
           merchant_name,
           total_canceled_amount,
           currency
    from top_merchant_has_most_canceled_order_value_by_year tmy
    where tmy.rn <= 10
    order by year desc, total_canceled_amount desc
);
"""


# Top 10 merchants that have new order value
CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_NEW_ORDER_VALUE_VALUE_BY_DAY = """
create table if not exists tamara_reports.top_10_merchant_has_most_new_order_value_by_day as (
    with top_merchant_has_new_order_value_by_day as (
        select *,
               ROW_NUMBER() over (
                   partition by day
                   order by total_new_order_amount desc
                   ) rn
        from (
                 select date(oi.created_at)        as day,
                        oi.merchant_name,
                        oi.order_currency          as currency,
                        sum(oi.order_total_amount) as total_new_order_amount
                 from tamara_reports.order_info oi
                 group by merchant_name, day, currency
             ) T
    )
    select day, merchant_name, total_new_order_amount, currency
    from top_merchant_has_new_order_value_by_day tmd
    where tmd.rn <= 10
    order by day desc, total_new_order_amount desc
);
"""

CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_NEW_ORDER_VALUE_VALUE_BY_MONTH = """
create table if not exists tamara_reports.top_10_merchant_has_most_new_order_value_by_month as (
    with top_merchant_has_new_order_value_by_month as (
        select *,
               ROW_NUMBER() over (
                   partition by month
                   order by total_new_order_amount desc
                   ) rn
        from (
                 select date_format(oi.created_at, '%Y-%m') as month,
                        oi.merchant_name,
                        oi.order_currency          as currency,
                        sum(oi.order_total_amount) as total_new_order_amount
                 from tamara_reports.order_info oi
                 group by merchant_name, month, currency
             ) T
    )
    select month, merchant_name, total_new_order_amount, currency
    from top_merchant_has_new_order_value_by_month tmm
    where tmm.rn <= 10
    order by month desc, total_new_order_amount desc
);
"""

CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_NEW_ORDER_VALUE_VALUE_BY_QUARTER = """
create table if not exists tamara_reports.top_10_merchant_has_most_new_order_value_by_quarter as (
    with top_merchant_has_new_order_value_by_quarter as (
        select *,
               ROW_NUMBER() over (
                   partition by quarter
                   order by total_new_order_amount desc
                   ) rn
        from (
                 select concat(year(oi.created_at), '-', quarter(oi.created_at)) as quarter,
                        oi.merchant_name,
                        oi.order_currency          as currency,
                        sum(oi.order_total_amount) as total_new_order_amount
                 from tamara_reports.order_info oi
                 group by merchant_name, quarter, currency
             ) T
    )
    select quarter, merchant_name, total_new_order_amount, currency
    from top_merchant_has_new_order_value_by_quarter tmq
    where tmq.rn <= 10
    order by quarter desc, total_new_order_amount desc
);
"""

CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_NEW_ORDER_VALUE_VALUE_BY_YEAR = """
create table if not exists tamara_reports.top_10_merchant_has_most_new_order_value_by_year as (
    with top_merchant_has_new_order_value_by_year as (
        select *,
               ROW_NUMBER() over (
                   partition by year
                   order by total_new_order_amount desc
                   ) rn
        from (
                 select year(oi.created_at)        as year,
                        oi.merchant_name,
                        oi.order_currency          as currency,
                        sum(oi.order_total_amount) as total_new_order_amount
                 from tamara_reports.order_info oi
                 group by merchant_name, year, currency
             ) T
    )
    select year, merchant_name, total_new_order_amount, currency
    from top_merchant_has_new_order_value_by_year tmy
    where tmy.rn <= 10
    order by year desc, total_new_order_amount desc
);
"""

# Top 10 items contribute most to the late fee
CREATE_TEMP_TABLE_AVG_LATE_FEE_PER_SKU = """
create temporary table tamara_reports.avg_late_fee_per_sku_in_order as (
    with number_skus_in_order as (
        select order_id,
               count(distinct sku) as total_skus_number
        from tamara_reports.detailed_item_order_info
        group by order_id
    )
    select olf.order_id,
           (olf.total_late_fee_amount / nso.total_skus_number) avg_late_fee_per_sku
    from tamara_reports.order_late_fee_amount olf
             left join number_skus_in_order nso on olf.order_id = nso.order_id
    where olf.total_late_fee_amount > 0
);
"""

CREATE_TABLE_TOP_10_ITEM_CONTRIBUTED_MOST_TO_LATE_FEE = """
create table if not exists tamara_reports.top_10_item_contributed_most_to_late_fee as (
    select dioi.sku,
           sum(alfs.avg_late_fee_per_sku) as total_sku_late_fee_amount
    from tamara_reports.avg_late_fee_per_sku_in_order alfs
             left join tamara_reports.detailed_item_order_info dioi 
                on alfs.order_id = dioi.order_id
    group by dioi.sku
    order by total_sku_late_fee_amount desc
    limit 10
);
"""
