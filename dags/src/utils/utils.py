import logging
from typing import Any

import pymysql
from airflow.models.connection import Connection
from pymysql import Connection as ConnectionType

logger = logging.getLogger()


def get_db_conn(conn_id: str) -> ConnectionType:
    conn_info = Connection.get_connection_from_secrets(conn_id=conn_id)
    conn = pymysql.connect(
        host=conn_info.host,
        port=conn_info.port,
        user=conn_info.login,
        password=conn_info.password,
        database=conn_info.schema,
    )
    return conn


def execute_query(conn: ConnectionType, query: str) -> Any:
    with conn.cursor() as cursor:
        cursor.execute(query=query)
        conn.commit()
