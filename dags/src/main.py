import logging
import os
import sys
from logging import config

from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.utils.dates import days_ago

sys.path.append(
    os.path.join(os.path.dirname(__file__), "../..{ds}".format(ds=os.path.sep))
)
from dags.src.utils.utils import get_db_conn, execute_query  # noqa: E402
from dags.src.utils.config import LOGGING_CONFIG  # noqa: E402
from dags.src.utils.queries import (  # noqa: E402
    CREATE_TABLE_TOTAL_LATE_FEE_BY_DAY,
    CREATE_TABLE_TOTAL_LATE_FEE_BY_MONTH,
    CREATE_TEMP_TABLE_ITEM_ORDER_INFO,
    CREATE_TEMP_TABLE_DETAILED_ITEM_ORDER_INFO,
    CREATE_TEMP_TABLE_ORDER_INFO,
    CREATE_TABLE_TOTAL_LATE_FEE_BY_QUARTER,
    CREATE_TABLE_TOTAL_LATE_FEE_BY_YEAR,
    CREATE_TEMP_TABLE_ORDER_LATE_FEE_AMOUNT,
    CREATE_TEMP_TABLE_AVG_LATE_FEE_PER_SKU,
    CREATE_TABLE_TOP_10_MOST_PURCHASED_ITEM_BY_DAY,
    CREATE_TABLE_TOP_10_MOST_PURCHASED_ITEM_BY_MONTH,
    CREATE_TABLE_TOP_10_MOST_PURCHASED_ITEM_BY_QUARTER,
    CREATE_TABLE_TOP_10_MOST_PURCHASED_ITEM_BY_YEAR,
    CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_CANCELED_ORDER_VALUE_BY_DAY,
    CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_CANCELED_ORDER_VALUE_BY_MONTH,
    CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_CANCELED_ORDER_VALUE_BY_QUARTER,
    CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_CANCELED_ORDER_VALUE_BY_YEAR,
    CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_NEW_ORDER_VALUE_VALUE_BY_DAY,
    CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_NEW_ORDER_VALUE_VALUE_BY_MONTH,
    CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_NEW_ORDER_VALUE_VALUE_BY_QUARTER,
    CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_NEW_ORDER_VALUE_VALUE_BY_YEAR,
    CREATE_TABLE_TOP_10_ITEM_CONTRIBUTED_MOST_TO_LATE_FEE,
)

DAG_ID = "build_reports"
TAMARA_CONN_ID = "TAMARA_DB_URL"
default_args = {
    "owner": "@NamNDH",
    "depends_on_past": False,
    "catchup": False,
    "start_date": days_ago(1),
    "email": ["hainam.nguyen.ict@gmail.com"],
    "email_on_failure": True,
    "email_on_retry": False,
}

dag = DAG(
    dag_id=DAG_ID,
    description="DAG that runs to get result for questions in recruitment test of Tamara",  # noqa
    schedule_interval="@daily",
    default_args=default_args,
    max_active_runs=1,
)

logger = logging.getLogger(DAG_ID)
config.dictConfig(LOGGING_CONFIG)


def create_top_10_most_purchased_items_reports():
    db_conn = get_db_conn(conn_id=TAMARA_CONN_ID)
    execute_query(conn=db_conn, query=CREATE_TEMP_TABLE_ITEM_ORDER_INFO)
    execute_query(conn=db_conn, query=CREATE_TEMP_TABLE_ITEM_ORDER_INFO)
    execute_query(
        conn=db_conn, query=CREATE_TEMP_TABLE_DETAILED_ITEM_ORDER_INFO
    )
    execute_query(
        conn=db_conn, query=CREATE_TABLE_TOP_10_MOST_PURCHASED_ITEM_BY_DAY
    )
    execute_query(
        conn=db_conn, query=CREATE_TABLE_TOP_10_MOST_PURCHASED_ITEM_BY_MONTH
    )
    execute_query(
        conn=db_conn, query=CREATE_TABLE_TOP_10_MOST_PURCHASED_ITEM_BY_QUARTER
    )
    execute_query(
        conn=db_conn, query=CREATE_TABLE_TOP_10_MOST_PURCHASED_ITEM_BY_YEAR
    )
    db_conn.close()


def create_top_10_merchant_has_most_new_order_value_reports():
    db_conn = get_db_conn(conn_id=TAMARA_CONN_ID)
    execute_query(conn=db_conn, query=CREATE_TEMP_TABLE_ORDER_INFO)
    execute_query(
        conn=db_conn,
        query=CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_NEW_ORDER_VALUE_VALUE_BY_DAY,  # noqa
    )
    execute_query(
        conn=db_conn,
        query=CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_NEW_ORDER_VALUE_VALUE_BY_MONTH,  # noqa
    )
    execute_query(
        conn=db_conn,
        query=CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_NEW_ORDER_VALUE_VALUE_BY_QUARTER,  # noqa
    )
    execute_query(
        conn=db_conn,
        query=CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_NEW_ORDER_VALUE_VALUE_BY_YEAR,  # noqa
    )
    db_conn.close()


def create_top_10_merchant_has_most_canceled_order_value_reports():
    db_conn = get_db_conn(conn_id=TAMARA_CONN_ID)
    execute_query(conn=db_conn, query=CREATE_TEMP_TABLE_ORDER_INFO)
    execute_query(
        conn=db_conn,
        query=CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_CANCELED_ORDER_VALUE_BY_DAY
        # noqa
    )
    execute_query(
        conn=db_conn,
        query=CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_CANCELED_ORDER_VALUE_BY_MONTH,  # noqa
    )
    execute_query(
        conn=db_conn,
        query=CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_CANCELED_ORDER_VALUE_BY_QUARTER,  # noqa
    )
    execute_query(
        conn=db_conn,
        query=CREATE_TABLE_TOP_10_MERCHANT_HAS_MOST_CANCELED_ORDER_VALUE_BY_YEAR,  # noqa
    )
    db_conn.close()


def create_top_10_item_has_contribute_most_to_late_fee_report():
    db_conn = get_db_conn(conn_id=TAMARA_CONN_ID)
    execute_query(conn=db_conn, query=CREATE_TEMP_TABLE_ITEM_ORDER_INFO)
    execute_query(
        conn=db_conn, query=CREATE_TEMP_TABLE_DETAILED_ITEM_ORDER_INFO
    )
    execute_query(conn=db_conn, query=CREATE_TEMP_TABLE_ORDER_LATE_FEE_AMOUNT)
    execute_query(conn=db_conn, query=CREATE_TEMP_TABLE_AVG_LATE_FEE_PER_SKU)
    execute_query(
        conn=db_conn,
        query=CREATE_TABLE_TOP_10_ITEM_CONTRIBUTED_MOST_TO_LATE_FEE,
    )
    db_conn.close()


def create_total_late_fee_amount_reports():
    db_conn = get_db_conn(conn_id=TAMARA_CONN_ID)
    execute_query(conn=db_conn, query=CREATE_TEMP_TABLE_ORDER_LATE_FEE_AMOUNT)
    execute_query(conn=db_conn, query=CREATE_TABLE_TOTAL_LATE_FEE_BY_DAY)
    execute_query(conn=db_conn, query=CREATE_TABLE_TOTAL_LATE_FEE_BY_MONTH)
    execute_query(conn=db_conn, query=CREATE_TABLE_TOTAL_LATE_FEE_BY_QUARTER)
    execute_query(conn=db_conn, query=CREATE_TABLE_TOTAL_LATE_FEE_BY_YEAR)
    db_conn.close()


create_top_10_most_purchased_items_reports_task = PythonOperator(
    task_id="create_top_10_most_purchased_items_reports",
    python_callable=create_top_10_most_purchased_items_reports,
    dag=dag,
)

create_top_10_item_has_contribute_most_to_late_fee_report_task = PythonOperator(  # noqa
    task_id="create_top_10_item_has_contribute_most_to_late_fee_report",
    python_callable=create_top_10_item_has_contribute_most_to_late_fee_report,
    dag=dag,
)

create_top_10_merchant_has_most_new_order_value_reports_task = PythonOperator(
    task_id="create_top_10_merchant_has_most_new_order_value_reports",
    python_callable=create_top_10_merchant_has_most_new_order_value_reports,
    dag=dag,
)

create_top_10_merchant_has_most_canceled_order_value_reports_task = PythonOperator(  # noqa
    task_id="create_top_10_merchant_has_most_canceled_order_value_reports",
    python_callable=create_top_10_merchant_has_most_canceled_order_value_reports,  # noqa
    dag=dag,
)

create_total_late_fee_amount_reports_task = PythonOperator(
    task_id="create_total_late_fee_amount_reports",
    python_callable=create_total_late_fee_amount_reports,
    dag=dag,
)
