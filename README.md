# Tamara Data Engineer Coding Challenge

## Requirements
Our data warehouse system was replicated from our application data (in real time). The payload for order events in our application is in json format which didn't optimize for analytics applications. So when we need query/aggregate fields in a JSON column, it will take a considerable time. And we can't add indexes to fields inside the JSON column to speed up the query.

So we need to create some pipelines to denormalize JSON fields in order_events tables. The new data structure should answer business questions by conducting simple queries:

- Top 10 most purchased items by day, month, quarter, year.
- Top 10 items that contributed most to the late fee.
- Top 10 merchants who have most new order value by day, month, quarter, year
- Top 10 merchants who have most canceled order value by day, month, quarter, year
- Total late fee amount collected by day, month, quarter, year.

---
### Acceptance Criteria
- Setup project structure + Docker + code linting + mypy
- Storage:
Input: MySQL
Output: Mysql
- Answer all critical business questions

### Good to have:
- Pipeline that support incremental update in real time
- Unit tests and integration tests for critical logic
- Architecture: Cloud native (K8s)
- Define CICD (github, gitlab,...)

---
# Submission

## Prerequisite
- `python3.9`, `pip`, `venv`
- `Docker`, `docker-compose`
- To install all dependencies for development:
```commandline
pip install -r requirements.txt
```
- To install `pre-commit-hooks` to format and test lint, and type the code before commit:
```commandline
pre-commit install
```
- To run the `docker-compose.yml` properly. Firstly, run these commandlines below:
```commandline
mdkir -p ./logs ./plugins
cp .env_sample .env
```
- To up the project which contains the code to export reports. Please run the commandline below:
```commandline
docker-compose up
```

## Achieve the result
- To access the `Airflow` WebUI, go to the url: `localhost:8080` with default user/pwd: `airflow/airflow`.
- Unpause the DAG named `build_reports` to let the DAG run.
- Check the output in `MySQL` DB built by at URI: `mysql://root:root@127.0.0.1:3306/tamara_reports` after the DAG runs successfully.

## Clean up
To stop and delete containers, delete volumes with database data and download images, run:
```commandline
docker-compose down --volumes --rmi all
```

## Project Report
- Pipeline:
  - `unit-test` job which runs `pytest-cov` in all test files and exports the reports to `Artifact` of the repo. However, there is no test file in my submission, so that I set `allow-failure` is `true`.
  - `type-test` job which run `mypy` to check static type of the code.
  - `lint-test` job which run `flake8` to check coding styles and conventions.
  - The all jobs need to succeed before code in MR can merge into `main` branch.
- I created `.pre-commit-hook-config` which is going to run `black` to format my code, `flake8` to check for lint and `mypy` to check the static types of my code before every commit I make, so I can focus on coding.
- I created one report/table per dimension `day`, `month`, `quarter`, `year` individually in each question. In my opinion, this makes better sense than composition dimension of `day`, `month`, `quarter`, `year`.
- I have chosen to use SQL to process the data in `MySQL` DB and wrote the result back to it because I hardly found any solutions to deal with Arabic languages in payload effectively in Python script/PySpark. For the SQL, I can not provide good unit tests for the code, so I skipped this question.
- For the incremental update processing, because I lack of the knowledge about this, so I can't provide a good solution for this question.
- Due to lack of experience, I can not provide a good implementation of IaC to deploy the project on K8S, so I also skipped this question.
